//
// Created by tzhou on 8/24/17.
//

#ifndef LLPARSER_DISCOPE_H
#define LLPARSER_DISCOPE_H

#include <ir/metaData.h>

/** DIScope represents a scope such as DIFile, DISubprogram
 *
 */
class DIScope: public MetaData {

};

#endif //LLPARSER_DISCOPE_H
