//
// Created by tzhou on 6/11/17.
//

#ifndef LLPARSER_INSTESSENTIAL_H
#define LLPARSER_INSTESSENTIAL_H

#include "instFlags.h"
#include "callInst.h"
#include "invokeInst.h"
#include "loadInst.h"
#include "bitcastInst.h"

#endif //LLPARSER_INSTESSENTIAL_H
